Taylor Swift
Lady Gaga
Justin Bieber
Ariana Grande
Bruno Mars

INSERT INTO artists 
VALUES ("Taylor Swift");

INSERT INTO artists 
VALUES ("Lady Gaga");

INSERT INTO artists 
VALUES ("Justin Bieber");

INSERT INTO artists 
VALUES ("Ariana Grande");

INSERT INTO artists 
VALUES ("Bruno Mars");

INSERT INTO albums (album_title, date_released, artist_id)
VALUES ("Fearless", "2008-01-01", 3);

INSERT INTO albums (album_title, date_released, artist_id)
VALUES ("Red", "2018-01-01", 3);

INSERT INTO albums (album_title, date_released, artist_id)
VALUES ("A Star Is Born", "2018-01-01", 4);

INSERT INTO albums (album_title, date_released, artist_id)
VALUES ("Born This Way", "2011-01-01", 4);

INSERT INTO albums (album_title, date_released, artist_id)
VALUES ("Purpose", "2015-01-01", 5);

INSERT INTO albums (album_title, date_released, artist_id)
VALUES ("Dangerous Woman", "2016-01-01", 6);

INSERT INTO songs (song_name, length, genre, album_id)
VALUES ("Fearless", 246, "Pop Rock", 32);

INSERT INTO songs (song_name, length, genre, album_id)
VALUES ("State of Grace", 253, "Rock", 33);

INSERT INTO songs (song_name, length, genre, album_id)
VALUES ("Black Eyes", 151, "Rock", 34);

INSERT INTO songs (song_name, length, genre, album_id)
VALUES ("Born This Way", 252, "Electropop", 33);

INSERT INTO songs (song_name, length, genre, album_id)
VALUES ("Sorry", 212, "Dancehall", 36);

INSERT INTO songs (song_name, length, genre, album_id)
VALUES ("Into You", 242, "EDM", 37);

SELECT songs.song_name, songs.length, albums.album_title, artists.name FROM songs
JOIN albums ON songs.album_id = albums.id
JOIN artists ON albums.artist_id = artists.id;